<?php

namespace Rashid\Perfectmoney;

use \GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Rashid\Perfectmoney\Contracts\ResponseParserServiceInterface;
use Rashid\Perfectmoney\Contracts\VoucherPerfectMoneyServiceInterface;
use Rashid\Perfectmoney\Exceptions\PerfectException;

class VoucherPerfectMoneyService implements VoucherPerfectMoneyServiceInterface
{

    private Client $restClient;
    private $responseParserService;
    private $accountId;
    private $payeeAccount;
    private $password;
    private $hasLog;
    private $hasDbLog;
    private $dbModelClass;

    /**
     * @param ResponseParserServiceInterface $responseParserService
     */
    public function __construct( ResponseParserServiceInterface $responseParserService)
    {
        $this->hasLog = config('voucher_perfectmoney.has_log');
        $this->hasDbLog = config('voucher_perfectmoney.has_db_log');
        $this->dbModelClass = config('voucher_perfectmoney.model_db_class');
        $this->responseParserService = $responseParserService;
        $config = [
            'base_url'        => config('voucher_perfectmoney.base_url'),
            'timeout'         => config('voucher_perfectmoney.timeout'),
            'proxy'         => config('voucher_perfectmoney.proxy'),
            'connect_timeout' => config('voucher_perfectmoney.connect_timeout')
        ];
        $this->setConfig(config('voucher_perfectmoney.account_id') ,config('voucher_perfectmoney.Payee_Account'),config('voucher_perfectmoney.password'));

        if ($this->accountId){
            throw new PerfectException("set config");
        }
        if (config('voucher_perfectmoney.proxy')){
            $config['proxy'] = config('voucher_perfectmoney.proxy');
        }
        $this->restClient = new Client($config);
    }

    /**
     * @param $activationCode
     * @param $voucherCode
     * @return array
     */
    public function checkCode($activationCode, $voucherCode): array
    {
        try {

            $requestData = [
                'AccountID'     => $this->accountId,
                'Payee_Account' => $this->payeeAccount,
                'PassPhrase'    => $this->password,
                'ev_number'     => $voucherCode,
                'ev_code'       => $activationCode
            ];
            // todo remove comment api
            if ($this->hasLog){
                Log::info('before voucher store' , ['request' => $requestData]);
            }
            $response = $this->restClient->request('GET', config('voucher_perfectmoney.url'), [
                'query' => $requestData
            ]);

            if ($response->getStatusCode() === 200) {
                $result = $this->responseParserService->htmlParser($html = $response->getBody()->getContents());
                if ($this->hasLog) {
                    Log::info('after voucher store ok html', ['request' => $html]);
                    Log::info('after voucher store ok json', ['request' => $result]);
                }
            } else {
                if ($this->hasLog) {
                    Log::info('after voucher store nok', ['request' => $response->getBody()->getContents()]);
                }
                $result = [
                    'status' => 'error',
                    'data'   => $response->getStatusCode()
                ];
            }

            if ($this->hasDbLog){
                $this->dbModelClass::create([
                'user_id'         => Auth::id() ?? null,
                'voucher_code'    => $voucherCode,
                'activation_code' => $activationCode,
                'batch_number'    => $result['data']['PAYMENT_BATCH_NUM']['value'] ?? null,
                'result'          => json_encode($result),
            ]);
            }

        }catch (\Throwable $throwable){
            $result = [
                'status' => 'networkError',
                'data'   => $throwable->getMessage()
            ];
        }
        return $result;
    }

    /**
     * @param $accountId
     * @param $payeeAccount
     * @param $password
     * @return $this
     */
    public function setConfig($accountId,$payeeAccount,$password)
    {
        $this->accountId = $accountId;
        $this->payeeAccount = $payeeAccount;
        $this->password = $password;
        return $this;
    }
}



