<?php

namespace  Rashid\Perfectmoney\Facades;

use Illuminate\Support\Facades\Facade;
class PerfectMoney extends Facade
{
    /**
     * @see \Rashid\Perfectmoney\VoucherPerfectMoneyService
     * @return string
     */
 protected static function getFacadeAccessor():string
 {
     return 'perfect-money';
 }

}

