<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_code_validated', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->naullable();
            $table->string('voucher_code');
            $table->string('activation_code');
            $table->json('result');
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->string('batch_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_code_validated');
    }
};
