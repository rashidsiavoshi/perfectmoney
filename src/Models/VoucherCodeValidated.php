<?php

namespace Rashid\Perfectmoney\Models;
use Illuminate\Database\Eloquent\Model;

class VoucherCodeValidated extends Model
{
    protected $guarded=['id'];
    protected $table = 'voucher_code_validated';
}
