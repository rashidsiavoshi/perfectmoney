<?php

namespace Rashid\Perfectmoney;


use Rashid\Perfectmoney\Contracts\ResponseParserServiceInterface;

class ResponseParserService implements ResponseParserServiceInterface
{
    /**
     * @param $response
     * @return array
     */
    public function HtmlParser($response): array
    {
        $doc = new \DOMDocument();
        $doc->loadHTML($response);
        $arr = $doc->getElementsByTagName("input");
        $inputs = [];
        $errors = [];
        foreach ($arr as $item) {
            $value = $item->getAttribute("value");
            $name = $item->getAttribute("name");

            if (strtolower($name) == 'error') {
                $errors[] = $value;
            }

            $inputs[$name] = [
                'name'  => $name,
                'value' => $value,
            ];
        }

        //  has error return error
        if (count($errors)) {
            return [
                'status' => 'error',
                'data'   => $errors
            ];
        }

        return [
            'status' => 'success',
            'data'   => $inputs
        ];
    }
}
