<?php

namespace Rashid\Perfectmoney;

use Illuminate\Support\ServiceProvider;
use Rashid\Perfectmoney\Contracts\ResponseParserServiceInterface;
use Rashid\Perfectmoney\Contracts\VoucherPerfectMoneyServiceInterface;

class PerfectMoneyServiceProvider extends ServiceProvider
{
    /**
     * All the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        ResponseParserServiceInterface::class                => ResponseParserService::class,
        VoucherPerfectMoneyServiceInterface::class          => VoucherPerfectMoneyService::class,
        ];
    /**
     * Register any application service
     * @return void
     */
    public function register()
    {

        $this->app->bind('perfect-money',function ($app){
            return new VoucherPerfectMoneyService(new ResponseParserService());
        });
    }

    /**
     * Bootstrap any application service
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/Config/voucher_perfectmoney.php' => config_path('voucher_perfectmoney.php')]);

        $this->loadMigrationsFrom(__DIR__.'/Database/Migration');

    }

}

