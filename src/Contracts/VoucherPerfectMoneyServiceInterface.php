<?php

namespace Rashid\Perfectmoney\Contracts;

interface VoucherPerfectMoneyServiceInterface {
    public function checkCode($activationCode,$voucherCode);
    public function setConfig($accountId,$payeeAccount,$password);

}
