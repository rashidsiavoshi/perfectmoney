<?php

namespace Rashid\Perfectmoney\Contracts;

interface ResponseParserServiceInterface {
    public function htmlParser($response);
}
