<?php

return [
    // when user want to sell voucher to us ,then we must check it
    'PAYEE_NAME'      => env('PAYEE_NAME'),
    'base_url'        => 'https://perfectmoney.com/acct',
    'check_code_url'  => 'ev_activate.asp',
    'url'             => env('PERFECT_URL'),
    'proxy'           => env('PERFECT_URL_PROXY'),
    'timeout'         => 30,
    'account_id'      => env('PERFECT_ACCOUNT_ID'),
    'Payee_Account'   => env('PERFECT_PAYEE_ACCOUNT'),
    'password'        => env('PERFECT_PASSWORD'),
    'has_db_log'      => true,
    'model_db_class'  => Rashid\Perfectmoney\Models\VoucherCodeValidated::class,
    'has_log'         => true,
    'connect_timeout' => 30,
];
