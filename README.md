


# Perfect money sell auto package

The `rashid/perfectmoney` package provides easy to use functions to log the activities perfect vouchers code

Here's a demo of how you can use it:

```php
\Rashid\Perfectmoney\Facades\PerfectMoney::checkCode('activationCode','voucherCode')
```

set config without env
```php
\Rashid\Perfectmoney\Facades\PerfectMoney::setConfig('accountId','payeeAccount','password')

```

env
```php
PAYEE_NAME=
PERFECT_ACCOUNT_ID=
PERFECT_PAYEE_ACCOUNT=
PERFECT_PASSWORD=
PERFECT_URL=
PERFECT_URL_PROXY=
```

## Installation

You can install the package via composer:

```bash
composer require rashid-sia/perfectmoney
```

The package will automatically register itself.

You can publish the migration with:

```bash
php artisan vendor:publish --provider="rashid/perfectmoney\PerfectMoneyServiceProvider" 
```
